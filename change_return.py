# change_return.py
# Programmer: Demar Brown (Demar Codes)
# Date: Oct 17, 2020
# Program Details: This program allows the user to enter the cost for an item and the amount tendered by the user.
    # The program should then figure out the change and the number of each type of dollars needed for the change.
    # Eg: $285 change may be offered as 2*$100, 1*$50, 2*$20 and 1*5

#-------------------
# ------------------------------INCOMPLETE: Cents do not calculate correctly. Otherwise, program runs successfully
x=0

def change_returned(cost, cash_tendered):
    
    change_dict = {
        "five_thousand": 0,
        "one_thousand": 0,
        "five_hundred": 0,
        "one_hundred": 0,
        "fifty": 0,
        "twenty": 0,
        "ten": 0,
        "five": 0,
        "one": 0,
        "ten_cent": 0,
        "one cent": 0
    }

    change= round(cash_tendered - cost, 2)

    print(change)

    while change>0:
        if change>5000:
            change_dict["five_thousand"] = change//5000
            change= round(change-5000*(change//5000))
            print(change)
        elif change>1000:
            change_dict["one_thousand"] = change//1000
            change= round(change-1000*(change//1000))
            print(change)
        elif change>500:
            change_dict["five_hundred"] = change//500
            change= round(change-500*(change//500))
            print(change)
        elif change>100:
            change_dict["one_hundred"] = change//100
            change= round(change-100*(change//100))
            print(change)
        elif change>50:
            change_dict["fifty"] = change//50
            change= round(change-50*(change//50))
            print(change)
        elif change>20:
            change_dict["twenty"] = change//20
            change= round(change-20*(change//20))
            print(change)
        elif change>10:
            change_dict["ten"] = change//10
            change= round(change-10*(change//10))
            print(change)
        elif change>5:
            change_dict["five"] = change//5
            change= round(change-5*(change//5))
            print(change)
        elif change>1:
            change_dict["one"] = change//1
            change= round(change-1*(change//1))
            print(change)
        elif change>0.10:
            change_dict["ten_cent"] = change//0.10
            change= round(change-0.10*(change//0.10))
            print(change)
        elif change>0.1:
            change_dict["one_cent"] = change//0.01
            change= round(change-0.01*(change//0.01))
            print(change)
        
    print("Five Thousands: " +str(change_dict.get("five_thousand")))
    print("One Thousands: "+str(change_dict.get("one_thousand")))
    print("Five Hundreds: " +str(change_dict.get("five_hundred")))
    print("One Hundreds: "+str(change_dict.get("one_hundred")))
    print("Fifties: " +str(change_dict.get("fifty")))
    print("Twenties: "+str(change_dict.get("twenty")))
    print("Tens: " +str(change_dict.get("ten")))
    print("Fives: " +str(change_dict.get("five")))
    print("Ones: "+str(change_dict.get("one")))
    print("Ten Cents: " +str(change_dict.get("ten_cent")))
    print("One Cents: "+str(change_dict.get("one_cent")))


#Program begins here

print("\nThis is a Change Return System\n")

response= int(input("How many items do you wish to check?"))

while x<response:
    cost=float(input("Enter cost for your next item: "))
    cash_tendered=float(input("Enter amount to be tendered: "))

    change_returned(cost, cash_tendered)
    x+=1
